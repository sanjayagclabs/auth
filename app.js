var express = require ('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var sessions = require('client-sessions');
var bcrypt = require('bcryptjs');
var app =express();
app.set('view engine', 'jade');
app.locals.pretty =true;

//connect to mongo
mongoose.connect('mongodb://localhost/newauth');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var User = mongoose.model('User', new Schema({
    id:           ObjectId,
    firstName:    { type: String, required: '{PATH} is required.' },
    lastName:     { type: String, required: '{PATH} is required.' },
    email:        { type: String, required: '{PATH} is required.', unique: true },
    password:     { type: String, required: '{PATH} is required.' },
    data:         Object,
}));

//middleware
app.use(bodyParser.urlencoded({extended: true}));

app.use(sessions({
    cookieName: 'session',
    secret: 'dfjkytfghvjlkgjggcghcghcjgh',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000
}));

app.use(function(req, res, next){
    if(req.session && req.session.user) {
        User.findOne({email: req.session.user.email}, function (err, user) {
            if (user) {
                req.user = user;
                delete req.user.password;
                res.locals.user = req.user;
            }
            next();
        });
    }
    else{
        next();
    }
});

function requireLogin(req, res, next){
    if (!req.user){
        res.redirect('/login');
    }else{
        next();
    }
}

app.get('/', function(req, res){
    res.render('index.jade');
});
app.get('/register', function(req, res){
    res.render('register.jade');
});
app.get('/dashboard', requireLogin, function(req, res){
    res.render('dashboard.jade');
});
app.get('/login', function(req, res){
    res.render('login.jade');
});
app.get('/logout', function(req, res){
    req.session.reset();
    res.redirect('/');
});

app.post('/register', function(req, res) {
    //var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));

    var user = new User({
        firstName:  req.body.firstName,
        lastName:   req.body.lastName,
        email:      req.body.email,
        password:   hash
    });
    user.save(function(err) {
        if (err) {
            var error = 'Something bad happened! Please try again.';

            if (err.code === 11000) {
                error = 'That email is already taken, please try another.';
            }

            res.render('register.jade', { error: error });
        } else {
            //utils.createUserSession(req, res, user);
            res.redirect('/dashboard');
        }
    });
});

app.post('/login', function(req, res) {
    User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
            res.render('login.jade', { error: "Incorrect email / password." });
        } else {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                //utils.createUserSession(req, res, user);
                req.session.user = user;
                res.redirect('/dashboard');
            } else {
                res.render('login.jade', { error: "Incorrect email / password."  });
            }
        }
    });
});


var server = app.listen(3005, function () {

    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);

});
